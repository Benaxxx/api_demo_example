export const storeKeys = (access_token, refresh_token) => {
  localStorage.setItem("access_token", access_token);
  localStorage.setItem("refresh_token", refresh_token);
};

export const deleteKeys = () => {
  if (localStorage.getItem("access_token")) {
    localStorage.removeItem("access_token");
  }
  if (localStorage.getItem("refresh_token")) {
    localStorage.removeItem("refresh_token");
  }
};

export const getAccessToken = () => {
  return localStorage.getItem("access_token") ?? "";
};

export const getRefreshToken = () => {
  return localStorage.getItem("refresh_token") ?? "";
};
