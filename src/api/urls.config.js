const BASE_URL = "http://127.0.0.1:8000/api/v1/";

export const URLS_CONFIG = {
  TOKEN_PREFIX: "JWT ",

  USERS: {
    LIST: `${BASE_URL}users/`,
    GET: `${BASE_URL}users/`,
    UPDATE: `${BASE_URL}users/`,
    CREATE: `${BASE_URL}users/`,
    DELETE: `${BASE_URL}users/`,
    GET_ME: `${BASE_URL}users/me/`,
    UPDATE_ME: `${BASE_URL}users/me/`,
    DELETE_ME: `${BASE_URL}users/me/`,
    RESET_EMAIL: `${BASE_URL}users/reset_email/`,
    RESET_EMAIL_CONFIRM: `${BASE_URL}users/reset_email_confirm/`,
    RESET_PASSWORD: `${BASE_URL}users/reset_password/`,
    RESET_PASSWORD_CONFIRM: `${BASE_URL}users/reset_password_confirm/`,
    SET_PASSWORD: `${BASE_URL}users/set_password/`
  },

  POSITIONS: {
    GET: `${BASE_URL}positions/`
  },

  AUTHORIZATION: {
    AUTHORIZE: `${BASE_URL}auth/jwt/create/`,
    REFRESH: `${BASE_URL}auth/jwt/refresh/`,
    VERIFY: `${BASE_URL}auth/jwt/verify/`
  }
};
