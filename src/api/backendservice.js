import axios from "axios";
import { URLS_CONFIG } from "./urls.config";
import { storeKeys, getAccessToken } from "./localstorageservice";

const APIServiceRequest = async (config, token_needed = true) => {
  try {
    config.headers = config.headers ?? {};
    if (token_needed) {
      config.headers.Authorization =
        URLS_CONFIG.TOKEN_PREFIX + getAccessToken();
    }

    const { data } = await axios(config);
    return ["ok", data];
  } catch (error) {
    return [
      "error",
      {
        status: error.response?.status,
        data: error.response?.data,
        message: error.message
      }
    ];
  }
};

export const authorize = async (email, password) => {
  const [status, data] = await APIServiceRequest(
    {
      method: "post",
      url: `${URLS_CONFIG.AUTHORIZATION.AUTHORIZE}`,
      data: { email, password }
    },
    false
  );

  if (status === "ok") {
    storeKeys(data.access, data.refresh);
  }

  return [status, data];
};

export const checkAuth = async () => {
  const [status, data] = await APIServiceRequest({
    method: "post",
    url: `${URLS_CONFIG.AUTHORIZATION.VERIFY}`,
    data: { token: getAccessToken() }
  });

  return [status, data];
};

export const resetPassword = async (email) => {
  return await APIServiceRequest(
    {
      method: "post",
      url: `${URLS_CONFIG.USERS.RESET_PASSWORD}`,
      data: { email }
    },
    false
  );
};

export const resetPasswordConfirmation = async (new_password, uid, token) => {
  return await APIServiceRequest(
    {
      method: "post",
      url: `${URLS_CONFIG.USERS.RESET_PASSWORD_CONFIRM}`,
      data: { new_password, uid, token }
    },
    false
  );
};

export const registerUser = async (
  email,
  first_name,
  last_name,
  middle_name,
  position
) => {
  return await APIServiceRequest({
    method: "post",
    url: `${URLS_CONFIG.USERS.CREATE}`,
    data: { email, first_name, last_name, middle_name, position }
  });
};

export const getCurrentUser = async () => {
  return await APIServiceRequest({
    method: "get",
    url: `${URLS_CONFIG.USERS.GET_ME}`
  });
};

export const getUserById = async (id) => {
  return await APIServiceRequest({
    method: "get",
    url: `${URLS_CONFIG.USERS.GET}${id}/`
  });
};

export const getUserPositions = async () => {
  return await APIServiceRequest({
    method: "get",
    url: `${URLS_CONFIG.POSITIONS.GET}`
  });
};
