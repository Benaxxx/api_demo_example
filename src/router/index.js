import { createRouter, createWebHistory } from "vue-router";
import EmptyLayout from "../layouts/EmptyLayout.vue";
import MainLayout from "../layouts/MainLayout.vue";
import { checkAuth } from "../api/backendservice";

const routes = [
  {
    path: "/login",
    name: "login",
    meta: {
      layout: EmptyLayout,
      auth: false
    },
    component: () => import("../views/LoginPage.vue")
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    meta: {
      layout: EmptyLayout,
      auth: false
    },
    component: () => import("../views/ForgotPasswordPage.vue")
  },
  {
    path: "/reset-password/:uid/:token",
    name: "reset-password",
    meta: {
      layout: EmptyLayout,
      auth: false
    },
    component: () => import("../views/ResetPasswordPage.vue")
  },
  {
    path: "/",
    redirect: {
      name: "group-list"
    },
    meta: {
      layout: MainLayout,
      auth: true
    }
  },
  {
    path: "/groups",
    name: "group-list",
    meta: {
      layout: MainLayout,
      auth: true
    },
    component: () => import("../views/GroupListPage.vue")
  },
  {
    path: "/students",
    name: "student-list",
    meta: {
      layout: MainLayout,
      auth: true
    },
    component: () => import("../views/StudentListPage.vue")
  },
  {
    path: "/users",
    name: "user-list",
    meta: {
      layout: MainLayout,
      auth: true
    },
    component: () => import("../views/UserListPage.vue")
  },
  {
    path: "/users/:id",
    name: "user-profile",
    meta: {
      layout: MainLayout,
      auth: true
    },
    component: () => import("../views/UserPage.vue")
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach(async (to, from, next) => {
  const requireAuth = to.matched.some((record) => record.meta.auth);
  if (requireAuth || (to.name === "login" && !to.params.checked)) {
    const response = await checkAuth();
    if (response[0] === "error") {
      next({ name: "login", params: { checked: true } });
      return;
    }
    if (to.name === "login") {
      next("/");
      return;
    }
  }
  next();
});

export default router;
