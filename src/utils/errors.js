export default {
  "User with given email does not exist.":
    "Пользователя с таким email не существует",
  "No active account found with the given credentials":
    "Неверный email или пароль",
  "Authentication credentials were not provided.":
    "Для начала войдите в систему",
  "You do not have permission to perform this action.":
    "У вас недостаточно прав для просмотра этой страницы",
  "user with this email adress already exists.":
    "Пользователь с таким email уже существует",
  "Enter a valid email address.": "Введите корректный Email",
  "Unexpected error": "Неизвестная ошибка",
  "Network Error": "Ошибка сети. Невозможно получить доступ к серверу",
  404: "Такой страницы не существует",
  500: "Произошла какая-то ошибка на сервере"
};
