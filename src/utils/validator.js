export const required = (val) => {
  return val.length > 0 || "Поле не должно быть пустым";
};

export const email = (email) => {
  const re =
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9]){1,}/;
  return re.test(email) || "Некорректный email";
};

export const password = (password) => {
  const re =
    /(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,}/;
  return (
    re.test(password) ||
    "Пароль должен содержать не менее 8 символов с использованием цифр, спец. символов, а также маленьких и больших букв латиницы."
  );
};

export const equalValues = (...values) => {
  return (
    values.every((v) => v === values[0]) || "Значения в полях должны совпадать"
  );
};
