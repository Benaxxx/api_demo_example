import { getCurrentUser } from "../api/backendservice";

export default {
  state: {
    user: undefined
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    clearUser(state) {
      state.user = {};
    }
  },
  actions: {
    async getUser({ commit, dispatch }) {
      const [status, data] = await getCurrentUser();
      if (status === "error") {
        dispatch("handleError", data);
        return "error";
      }

      commit("setUser", data);
      return "ok";
    }
  },
  getters: {
    user: (s) => s.user
  }
};
