import { createStore } from "vuex";
import errors from "../utils/errors";
import user from "./user";
import { deleteKeys } from "../api/localstorageservice"
//import router from "../router";

export default createStore({
  state: {
    error: undefined,
    popupMessage: undefined
  },
  mutations: {
    setError(state, error) {
      state.error = error;
    },
    clearError(state) {
      state.error = undefined;
    },
    popupMessage(state, message) {
      state.message = message;
    },
    clearPopupMessage(state) {
      state.message = undefined;
    }
  },
  actions: {
    handleError({ commit }, error) {
      // TODO: унифицировать синтаксисов ответа при ошибке, избавиться от костылей (Помнить: при недоступности сервера есть только message)
      const { status = undefined, data = [], message } = error;
      let errMessage =
        errors[data?.detail] ||
        errors[data?.email] ||
        errors[data[0]] ||
        errors[status] ||
        errors[message];
      if (!errMessage) {
        errMessage = errors["Unexpected error"];
      }
      commit("setError", errMessage);
    },
    logoutClear() {
      deleteKeys();
    }
  },
  getters: {
    error: (state) => state.error,
    popupMessage: (state) => state.popupMessage
  },
  modules: {
    user
  }
});
